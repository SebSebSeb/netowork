system {
    host-name SRXA1;
    root-authentication {
        encrypted-password "$1$xH9xJoL6$MFOUYnZr4.Qj2NM24XInz/"; ## Password: Rootpass
    }
    services {
        ssh;
        dhcp-local-server {
            group NET10 {
                interface ge-0/0/3.0;
            }
            group NET11 {
                interface ge-0/0/2.0;
            }
        }
    }
}
interfaces {
    ge-0/0/3 {
        unit 0 {
            family inet {
                address 192.168.10.1/24;
            }
        }
    }
    ge-0/0/2 {
        unit 0 {
            family inet {
                address 192.168.12.1/24;
            }
        }
    }
}
routing-options {
    static {
        route 192.168.11.0/24 next-hop 192.168.12.2;
		route 0.0.0.0/24 next-hop 192.168.12.2;
		route 0.0.0.0/32 next-hop 192.168.12.2;
		route 0.0.0.0/0 next-hop 192.168.12.2;
    }
}
security {
    policies {
        from-zone trust to-zone trust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
        from-zone trust to-zone untrust {
            policy default-permit {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    permit;
                }
            }
        }
		from-zone untrust to-zone trust {
            policy default-deny {
                match {
                    source-address any;
                    destination-address any;
                    application any;
                }
                then {
                    deny;
                }
            }
        }
    }
    zones {
        security-zone trust {
			tcp-rst;
            interfaces {
                ge-0/0/2.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
                ge-0/0/3.0 {
                    host-inbound-traffic {
                        system-services {
                            all;
                        }
                    }
                }
            }
        }
        security-zone untrust;
    }
}
access {
    address-assignment {
        pool NET10 {
            family inet {
                network 192.168.10.0/24;
                range USERS {
                    low 192.168.10.5;
                    high 192.168.10.10;
                }
                dhcp-attributes {
                    maximum-lease-time 3600;
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        192.168.10.1;
                    }
                }
            }
        }
        pool NET11 {
            family inet {
                network 192.168.11.0/24;
                range COMPUTERS {
                    low 192.168.11.5;
                    high 192.168.11.10;
                }
                dhcp-attributes {
                    maximum-lease-time 3600;
                    name-server {
                        8.8.8.8;
                    }
                    router {
                        192.168.11.1;
                    }
                }
            }
        }
    }
}
